<?php


require_once(dirname( __FILE__ ).'/../../../wp-config.php');
require_once(dirname( __FILE__ ).'/../../../wp-load.php');
/*
* Plugin Name: dc_tracking
* Description: Desarrollado para poder hacer segumiento a un numero de guia y saber el paquete donde se encuentra
* Version: 1.0
* Author: DiegoCardona(diego0123@gmail.com)
* Autor Uri: http://thedeveloper.co
*/

/* helpers */
function dc_tracking_get_url(){
	$plugin_dir = plugin_dir_path( __FILE__ );
	return plugin_dir_path( __FILE__ );
}


/*******************/

function dc_tracking_maps_js_include()
{

	wp_enqueue_script('script.js', WP_PLUGIN_URL. '/dc_tracking/js/script.js' );
}

add_action( 'wp_enqueue_scripts', 'dc_tracking_maps_js_include' );



function dc_tracking_shortcode() { 
    define('DC_TRACKING_PLUGUIN_NAME','dc_tracking');
    include_once dc_tracking_get_url() .'view/main_page.php'; 
}
add_shortcode('dc-tracking-shortcode', 'dc_tracking_shortcode');

?>