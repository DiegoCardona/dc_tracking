jQuery(document).ready(function(){
	jQuery('#dc_tracking_back_btn').css('display', 'none');
	jQuery('#dc_tracking_track').on('click', function(){
		dc_tracking_service();
	})

});


var dc_tracking_service = function(){

	var code = jQuery("#dc_tracking_code").val().trim();
	var _url = jQuery("#dc_tracking_url").val() + "/dc_tracking/controller/api.php";
	_url = _url + "?code="+code+"&action=rastrear";
	//_url = _url + "?code=1159253&action=rastrear";

	jQuery.ajax({
	        type: "GET",
	        dataType: "json",
	        url: _url,
	        success: function(data){
	        	if(data.status == 'OK'){
	        		dc_tracking_show(data, code);
	        	}else{
	        		
	        	}
	        },
	        error: function(e){
	            console.log(e.message);
	        }
	});
}

var dc_tracking_show = function(data, code){

	var datos = data.json.trazabilidad;

	if(datos.length != 0){

		var table = estado_actual(data);

		table += '<h5 style="color:white;">Historial</h5>';
		table += '<div class="dc_row" style="color:white;">';
		table +=' 	<div class="dc_column cotizador_row_table" style="width: 100%">';
		table +=' 		<div class="divTable" style="border: 1px solid #000;">';
		table +=' 			<div class="divTableBody">';
		table +=' 				<div class="divTableRow">';
		table +=' 					<div class="divTableCell">CÓDIGO</div>';
		table +=' 					<div class="divTableCell">ESTADO</div>';
		table +=' 					<div class="divTableCell">FECHA</div>';
		table +=' 				</div>';

		for (var i = 0; i < datos.length; i++) {
			elemento = datos[i];
			fecha = elemento.fecha.split('T')[0];
			j = i + 1;
		table +=' 				<div class="divTableRow">';
		table +=' 					<div class="divTableCell">'+j+'</div>';
		table +=' 					<div class="divTableCell">'+elemento.estado+'</div>';
		table +=' 					<div class="divTableCell">'+fecha+'</div>';
		table +=' 				</div>';
		}
		table +=' 			</div>';
		table +=' 		</div>';
		table += '<br><div class="vc_btn3-container vc_btn3-inline" id="dc_tracking_back"><button style="z-index:1000;" class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-rounded vc_btn3-style-modern vc_btn3-color-green">Regresar</button></div>';
		table +=' 	</div>';
		table +=' </div>';



		jQuery('#dc_tracking_table').html(table);
	}else{
		jQuery('#dc_tracking_table').html(estado_no_results(datos, code));
	}
	jQuery('#dc_tracking_back_btn').css('display', ''); 
	jQuery('#dc_tracking_busqueda').css('display', 'none');
	jQuery('#dc_tracking_results').css('display', '');

	jQuery('#dc_tracking_back').on('click', function(){
		jQuery("#dc_tracking_code").val("");
		jQuery('#dc_tracking_results').css('display', 'none');
		jQuery('#dc_tracking_back_btn').css('display', 'none');
		jQuery('#dc_tracking_busqueda').css('display', '');		
	})	
	
}

var estado_actual = function(data){
	var datos = data.json.trazabilidad;
	var elemento = datos[datos.length-1];
	var table = '<div class="dc_row" style="color:white;">';
	table +=' 	<div class="dc_column cotizador_row_table" style="width: 100%; height: 50px!important">';
	table +=' 		<div class="divTable" style="border: 1px solid #000;">';
	table +=' 			<div class="divTableBody">';
	table +=' 				<div class="divTableRow">';
	table +=' 					<div class="divTableCell" style="font-size:17px;">ESTADO ACTUAL</div>';
	table +=' 					<div class="divTableCell">'+elemento.estado+'</div>';
	table +=' 				</div>';
	table +=' 			</div>';
	table +=' 		</div>';
	table +=' 	</div>';
	table +=' </div>';
	return table;
}
 
var estado_no_results = function(code){
	var table = '<div class="dc_row" style="color:white;">';
	table +=' 	<div class="dc_column cotizador_row_table" style="width: 100%; height: 50px!important">';
	table +=' 		<div class="divTable" style="border: 1px solid #000;">';
	table +=' 			<div class="divTableBody">';
	table +=' 				<div class="divTableRow">';
	table +=' 					<div class="divTableCell">No hay resultados para el código ingresado '+code+'</div>';
	table +=' 				</div>';
	table +=' 			</div>';
	table +=' 		</div>';
	table += '<br><div class="vc_btn3-container vc_btn3-inline" id="dc_tracking_back"><button style="z-index:1000;" class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-rounded vc_btn3-style-modern vc_btn3-color-danger">Regresar</button></div>';
	table +=' 	</div>';
	table +=' </div>';
	return table;
}


